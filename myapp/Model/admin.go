package Model

import "myapp/dataStore/postgres"

type Admin struct {
	Firstname string
	Lastname  string
	Email     string
	Password  string
}

const queryInsertAdmin = "INSERT into admin(firstname, lastname, email, password) VALUES ($1, $2, $3, $4);"

func (adm *Admin) Create() error {
	_, err := postgres.Db.Exec(queryInsertAdmin, adm.Firstname, adm.Lastname, adm.Email, adm.Password)
	return err
}

// login
type Login struct {
	Email    string
	Password string
}

const queryGetAdmin = "SELECT email, password FROM admin WHERE email = $1 and password = $2;"

func (adm *Login) Get() error {
	return postgres.Db.QueryRow(queryGetAdmin, adm.Email, adm.Password).Scan(&adm.Email, &adm.Password)
}
