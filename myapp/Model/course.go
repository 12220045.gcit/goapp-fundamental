package Model

import "myapp/dataStore/postgres"

type Course struct {
	Courseid   string `json:"courseid"`
	CourseName string `json:"coursename"`
}

const courseinsert = "INSERT INTO course(courseid, coursename)VALUES($1,$2);"

func (c *Course) Create() error {
	_, err := postgres.Db.Exec(courseinsert, c.Courseid, c.CourseName)
	return err
	// return postgres.Db.Exec(queryInsertUser, s.stdId, s.FirstName, s.LastName, s.email)
}

var queryGetCourse = "SELECT courseid, coursename FROM course WHERE courseid=$1;"

func (c *Course) Read() error {
	return postgres.Db.QueryRow(queryGetCourse, c.Courseid).Scan(&c.Courseid, &c.CourseName)
}

var queryUpdateCourse = "UPDATE course SET courseid = $1, coursename = $2 WHERE courseid = $3;"

func (c *Course) Update(oldID string) error {
	_, err := postgres.Db.Exec(queryUpdateCourse, c.Courseid, c.CourseName, oldID)
	return err
}

var queryDeleteCourse = "DELETE FROM course WHERE courseid=$1;"

func (c *Course) Delete() error {
	if _, err := postgres.Db.Exec(queryDeleteCourse, c.Courseid); err != nil {
		return err
	}
	return nil
}

func GetAllCourses() ([]Course, error) {
	rows, getErr := postgres.Db.Query("SELECT * FROM course;")
	if getErr != nil {
		return nil, getErr
	}
	courses := []Course{}

	for rows.Next() {
		var c Course
		dbErr := rows.Scan(&c.Courseid, &c.CourseName)
		if dbErr != nil {
			return nil, dbErr
		}
		courses = append(courses, c)
	}
	rows.Close()
	return courses, nil
}
