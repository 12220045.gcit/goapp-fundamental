CREATE TABLE admin (
    FirstName varchar(45) NOT NULL,
    LastName varchar(45) DEFAULT NULL,
    Email varchar(45) NOT NULL,
    Password varchar(45) NOT NULL,
    PRIMARY KEY (Email)
)

CREATE TABLE login (
    Email varchar(45) NOT NULL,
    Password varchar(45) NOT NULL,
    PRIMARY KEY (Email)
)

drop table enroll;
drop table course;
drope table enroll;

CREATE TABLE course (
courseid varchar(45) NOT NULL,
coursename varchar(45) NOT NULL,
PRIMARY KEY (courseid)
)

CREATE TABLE enroll (
std_id int NOT NULL,
course_id varchar(45) NOT NULL,
date_enrolled varchar(45) DEFAULT NULL,
PRIMARY KEY (std_id, course_id),
CONSTRAINT course_fk FOREIGN KEY (course_id) REFERENCES course
(courseid) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT std_fk FOREIGN KEY (std_id) REFERENCES student (StdId) ON
DELETE CASCADE ON UPDATE CASCADE
)